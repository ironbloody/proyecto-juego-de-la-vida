import random


def impresora(matriz):
    texto = ''
    for i in range(int(len(matriz[0]))):
        for j in range(int(len(matriz))):
            texto += matriz[i][j] + ' '
        texto += '\n'
    print(texto)

def impresorat(temp):
    texto = ''
    for i in range(int(len(temp[0]))):
        for j in range(int(len(temp))):
            texto += temp[i][j] + ' '
        texto += '\n'
    print(texto)



def contadores(N, matriz):
    contadorvivas = 0
    for i in range(N):
        for j in range(N):
            if matriz[i][j].count('x') == 1:
                contadorvivas += 1
    print("Celulas vivas: ", contadorvivas)

    contadormuertas = 0
    for i in range(N):
        for j in range(N):
            if matriz[i][j].count('.') == 1:
                contadormuertas += 1
    print("Celulas muertas: ", contadormuertas)


def vecinos(matriz, temp):
    contadorvecinos = 0
    for i in range(-1, 2):
        for j in range(-1, 2):

            if matriz[i - 1][j - 1] != 'x' or '.':
                matriz[i - 1][j - 1] = 0
                if matriz[i-1][j-1] == 'x':
                    contadorvecinos += 1

            if matriz[i - 1][j] != 'x' or '.':
                matriz[i - 1][j] = 0
                if matriz[i-1][j] == 'x':
                    contadorvecinos += 1

            if matriz[i - 1][j + 1] != 'x' or '.':
                matriz[i - 1][j + 1] = 0
                if matriz[i-1][j+1] == 'x':
                    contadorvecinos += 1

            if matriz[i][j - 1] != 'x' or '.':
                matriz[i][j - 1] = 0
                if matriz[i][j-1] == 'x':
                    contadorvecinos += 1

            if matriz[i + 1][j + 1] != 'x' or '.':
                matriz[i + 1][j + 1] = 0
                if matriz[i+1][j+1] == 'x':
                    contadorvecinos += 1

            if matriz[i][j + 1] != 'x' or '.':
                matriz[i][j + 1] = 0
                if matriz[i][j+1] == 'x':
                    contadorvecinos += 1

            if matriz[i + 1][j - 1] != 'x' or '.':
                matriz[i + 1][j - 1] = 0
                if matriz[i+1][j-1] == 'x':
                    contadorvecinos += 1

            if matriz[i + 1][j] != 'x' or '.':
                matriz[i + 1][j] = 0
                if matriz[i+1][j] == 'x':
                    contadorvecinos += 1

            if matriz[i][j] == 'x' and contadorvecinos == 2 or 3:
                temp[i][j] = 'x'
            if matriz[i][j] == 'x' and contadorvecinos > 3:
                temp[i][j] = '.'
            if matriz[i][j] == 'x' and contadorvecinos == 1 or 0:
                temp[i][j] = '.'
            if matriz[i][j] == '.' and contadorvecinos == 3:
                temp[i][j] = 'x'
    print("cambiada")
    impresorat(temp)


def crearmatriz():
    matriz = []
    celulas = ['x', '.']
    N = random.randint(3, 4)
    for i in range(N):
        matriz.append([])
        for j in range(N):
            matriz[i].append(random.choice(celulas))

    temp = []
    for i in range(N):
        temp.append([])
        for j in range(N):
            temp[i].append(matriz[i][j])

    print("original")
    impresora(matriz)
    contadores(N, matriz)
    vecinos(matriz, temp)


crearmatriz()
