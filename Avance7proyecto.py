import random
def impresora(matriz):
    texto=''
    for i in range(int(len(matriz[0]))):
        for j in range(int(len(matriz))):
            texto += matriz[i][j] + ' '
        texto += '\n'
    print(texto)

def contadores(N, matriz):
    contadorvivas = 0
    for i in range(N):
        for j in range(N):
            if matriz[i][j].count('x') == 1:
                contadorvivas += 1
    print("Celulas vivas: ", contadorvivas)

    contadormuertas = 0
    for i in range(N):
        for j in range(N):
            if matriz[i][j].count('.') == 1:
                contadormuertas += 1
    print("Celulas muertas: ", contadormuertas)

def crearmatriz():
    matriz = []
    celulas = ['x', '.']
    N = random.randint(3, 10)
    for i in range(N):
        matriz.append([])
        for j in range(N):
            matriz[i].append(random.choice(celulas))
    for i in range(N):
        print(' '.join(matriz[i]))
    contadores(N, matriz)

    for i in range(-1, 2):
        for j in range(-1, 2):
            contadorvecinos = 0
            if matriz[i][j] == 'x' and matriz[i-1][j-1] == 'x' and i-1 >= 0 and j-1 >= 0 and i-1 <= N-1 and j-1 <= N-1:
                contadorvecinos += 1
            if matriz[i][j] == 'x' and matriz[i-1][j] == 'x' and i-1 >= 0 and j >= 0 and i-1 <= N-1 and j <= N-1:
                contadorvecinos += 1
            if matriz[i][j] == 'x' and matriz[i-1][j+1] == 'x' and i-1 >= 0 and j+1 >= 0 and i-1 <= N-1 and j+1 <= N-1:
                contadorvecinos += 1
            if matriz[i][j] == 'x' and matriz[i][j-1] == 'x' and i >= 0 and j-1 >= 0 and i <= N-1 and j-1 <= N-1:
                contadorvecinos += 1
            if matriz[i][j] == 'x' and matriz[i+1][j+1] == 'x' and i+1 >= 0 and j+1 >= 0 and i+1 <= N-1 and j+1 <= N-1:
                contadorvecinos += 1
            if matriz[i][j] == 'x' and matriz[i][j+1] == 'x' and i >= 0 and j+1 >= 0 and i <= N-1 and j+1 <= N-1:
                contadorvecinos += 1
            if matriz[i][j] == 'x' and matriz[i+1][j-1] == 'x' and i+1 >= 0 and j-1 >= 0 and i+1 <= N-1 and j-1 <= N-1:
                contadorvecinos += 1
            if matriz[i][j] == 'x' and matriz[i+1][j] == 'x' and i+1 >= 0 and j >= 0 and i+1 <= N-1 and j <= N-1:
                contadorvecinos += 1

            if contadorvecinos == 2 or 3:
                matriz[i][j] = 'x'
            if contadorvecinos >= 4:
                matriz[i][j] = '.'
            if contadorvecinos <= 1:
                matriz[i][j] = '.'
            if matriz[i][j] == '.' and contadorvecinos == 3:
                matriz[i][j] = 'x'

    impresora(matriz)
    contadores(N, matriz)
crearmatriz()